# ZPR Group - Zoo task

***

## Table of contents

* [General info](#general-info)
    * [Features](#features)
* [Technologies](#technologies)
* [Setup](#setup)
* [Author](#author)

## General info

* The zoo consists of different zones and the animals that live in them. 
* A given animal inhabits only one zone. In a given zone different animals live. 
* The animal needs food and has its own name.
* An elephant eats 20, a lion 11 and a rabbit 4 units of food per day.
* The zone can hold 100 units of food per day.

### Features

* Application does not work in the browser.
* There are 6 endpoints that perform specific tasks.
* Run application and open Postman.
    * Click on <font size="5">+</font> to open new tab,<br>
      * change HTTP method from GET to POST and enter request URL: http://localhost:8080
      * click on Body, select raw and JSON format<br>
        * Create zone on "/api/zones" endpoint
          * format to create zone:<br>
            {"species":"x"}
          * put it in Body and Send
        * Create animal on "/api/animals" endpoint
          * format to create animal:<br>
            {"name":"x","species":"x"}
          * put it in Body and Send
    * Open new tab on <font size="5">+</font> and enter request URL: http://localhost:8080
        * Collecting animals from a given zone on "/api/zones/{id}" endpoint
            * need to input zone id in URL /api/zones/zoneId
        * Collecting animal with a given name on "/api/animals/{name}"
            * need to input animal name in URL /api/animals/animalName
        * Collecting report which zone needs the most feed on "api/zones/food" endpoint
        * Collecting report in which zone the fewest animals live on "api/zones" endpoint
* All other endpoints are unavailable
* There is validation when entering data
* Access to the database via a browser on localhost:8080/h2-console/
  * login: admin , password : admin

## Technologies

Project is created with:

* IntelliJ IDEA 2022.2.3 (Ultimate Edition)
* Java JDK version 17.0.2 [Link][1]
* Maven version 3.8.6 [Link][2]
* Postman version 10.5.6 [Link][3]
* org.springframework.boot version 3.0.0
* org.hibernate version 8.0.0.Final
* com.h2database version 2.1.214
* org.projectlombok version 1.18.24

## Setup

1. To run this project, install Java JDK from the link [JDK][1]

2. Install it locally using cmd:

* $ cd ../selectedDirectory
* $ git clone https://gitlab.com/KS-GrupaZPR/ZooTask.git (with https)
* $ git clone git@gitlab.com:KS-GrupaZPR/ZooTask.git (with ssh)<br>

3. Open project in your IDE (Intellij recommended).
    * Reload Maven to install required libraries.<br>
      * Note: The project uses external libraries that are downloaded from 
      an external maven repository.
      * Note: The database schema and tables will be created automatically 
      on application start and dropped when application end.
   
## Author

Kamil Stencel

[1]:https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html]

[2]:https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.zip

[3]:https://www.postman.com/downloads/



