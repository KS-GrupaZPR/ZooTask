package zpr.zootask;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ZooTaskApplicationTests {

    @Test
    @DisplayName("shouldPassIfSpringContextLoadCorrectly")
    void contextLoads() {
    }

}
