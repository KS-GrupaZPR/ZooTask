package zpr.zootask.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.dto.ZoneDto;
import zpr.zootask.data.entity.AnimalEntity;
import zpr.zootask.data.entity.ZoneEntity;
import zpr.zootask.data.form.AddAnimalForm;
import zpr.zootask.data.mapper.AnimalMapper;
import zpr.zootask.data.mapper.ZoneMapper;
import zpr.zootask.enums.SpeciesEnum;
import zpr.zootask.exceptions.*;
import zpr.zootask.repository.AnimalRepository;
import zpr.zootask.repository.ZoneRepository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class AnimalServiceImplTest {

    private final String testAnimalName = "animal";
    private final SpeciesEnum testAnimalSpecies = SpeciesEnum.LION;
    private final ZoneDto testZone = ZoneDto.builder().species(testAnimalSpecies).build();
    private final AnimalDto testAnimal = AnimalDto.builder().name(testAnimalName)
            .type(testAnimalSpecies.name())
            .zone(ZoneMapper.dtoToEntity(testZone)).build();
    private final AddAnimalForm testAnimalForm = new AddAnimalForm(testAnimalName, testAnimalSpecies.name());

    @InjectMocks
    AnimalServiceImpl animalService;
    @Mock
    ZoneRepository zoneRepository;
    @Mock
    AnimalRepository animalRepository;

    public AnimalServiceImplTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("shouldCorrectlyGetAnimalWhenAnimalNameExists")
    void getAnimalWithName_animalNameExists() throws AnimalNameNotExistsException {
        //given
        when(animalRepository.existsByName(Mockito.anyString())).thenReturn(true);
        when(animalRepository.findByName(Mockito.anyString())).thenReturn(AnimalMapper.dtoToEntity(testAnimal));

        //when
        final var animal = animalService.getAnimalWithName(testAnimalName);

        //then
        assertEquals(animal.getName(), testAnimalName);
    }

    @Test
    @DisplayName("shouldThrowAnimalNameNotExistsExceptionWhenAnimalNameNotExists")
    void getAnimalWithName_animalNameNotExistsException() {
        //given

        //when + then
        assertThrows(AnimalNameNotExistsException.class, () -> animalService.getAnimalWithName(testAnimalName));
    }

    @Test
    @DisplayName("shouldCorrectlyAddAnimalToZone")
    void addAnimal_correctAnimalNameAndNotExists_zoneExists_animalSpeciesMatchZoneSpecies()
            throws AnimalNameExistsException, IncorrectSpeciesException, ZoneNotFoundException, WrongInputDataException {
        //given
        when(zoneRepository.existsBySpecies(testAnimalSpecies)).thenReturn(true);
        when(zoneRepository.findFirstBySpeciesAndBlockZoneFalse(testAnimalSpecies)).thenReturn(ZoneMapper.dtoToEntity(testZone));
        when(animalRepository.saveAndFlush(Mockito.any(AnimalEntity.class))).thenReturn(AnimalMapper.dtoToEntity(testAnimal));
        when(zoneRepository.saveAndFlush(Mockito.any(ZoneEntity.class))).thenReturn(ZoneMapper.dtoToEntity(testZone));

        //when
        final var addedAnimal = animalService.addAnimal(testAnimalForm);

        //then
        assertFalse(addedAnimal.isEmpty());
    }

    @Test
    @DisplayName("shouldThrowWrongInputDataExceptionWhenAnimalFormIsNull")
    void addAnimal_animalFormIsNull_wrongInputDataException() {
        //given

        //when + then
        assertThrows(WrongInputDataException.class, () -> animalService.addAnimal(null));
    }

    @Test
    @DisplayName("shouldThrowZoneNotFoundExceptionWhenAvailableZoneNotMatchAnimalSpeciesOrZoneNotExists")
    void addAnimal_animalFormOk_zoneNotFoundException() {
        //given

        //when + then
        assertThrows(ZoneNotFoundException.class, () -> animalService.addAnimal(testAnimalForm));
    }

    @Test
    @DisplayName("shouldThrowAnimalNameExistsExceptionWhenAnimalNameExistsInDatabase")
    void addAnimal_animalFormOk_zoneExists_animalSpeciesMatchZoneSpecies_animalNameExistsException() {
        //given
        when(zoneRepository.existsBySpecies(testAnimalSpecies)).thenReturn(true);
        when(zoneRepository.findFirstBySpeciesAndBlockZoneFalse(testAnimalSpecies)).thenReturn(ZoneMapper.dtoToEntity(testZone));
        when(animalRepository.existsByName(Mockito.anyString())).thenReturn(true);

        //when + then
        assertThrows(AnimalNameExistsException.class, () -> animalService.addAnimal(testAnimalForm));
    }
}