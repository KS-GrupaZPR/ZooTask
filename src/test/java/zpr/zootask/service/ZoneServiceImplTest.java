package zpr.zootask.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.dto.ZoneDto;
import zpr.zootask.data.entity.ZoneEntity;
import zpr.zootask.data.form.AddZoneForm;
import zpr.zootask.data.mapper.ZoneMapper;
import zpr.zootask.enums.SpeciesEnum;
import zpr.zootask.exceptions.*;
import zpr.zootask.repository.ZoneRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class ZoneServiceImplTest {

    private final long testZoneId = 1;
    private final long oneZoneExists = 1;
    private final String testAnimalName = "animal";
    private final SpeciesEnum testAnimalSpecies = SpeciesEnum.RABBIT;
    private final ZoneDto testZone = ZoneDto.builder().species(testAnimalSpecies).build();
    private final List<ZoneDto> testEmptyZoneList = List.of(testZone);
    private final AnimalDto testAnimal = AnimalDto.builder()
            .name(testAnimalName)
            .type(testAnimalSpecies.name())
            .build();
    private final ZoneDto testZoneWithAnimals = ZoneDto.builder()
            .species(testAnimalSpecies)
            .animals(List.of(testAnimal, testAnimal, testAnimal)).build();
    private final List<ZoneDto> testZoneWithAnimalList = List.of(testZoneWithAnimals);

    @InjectMocks
    ZoneServiceImpl zoneService;
    @Mock
    ZoneRepository zoneRepository;

    public ZoneServiceImplTest() {
        MockitoAnnotations.openMocks(this);
    }

    @ParameterizedTest
    @EnumSource(SpeciesEnum.class)
    @DisplayName("shouldCorrectlyAddZone")
    void addZone(final SpeciesEnum species) throws IncorrectSpeciesException, WrongInputDataException {
        //given
        final var zoneForm = new AddZoneForm(species.name());
        final var testedZone = ZoneDto.builder().species(species).build();
        when(zoneRepository.saveAndFlush(Mockito.any(ZoneEntity.class))).thenReturn(ZoneMapper.dtoToEntity(testedZone));

        //when
        final var addedZone = zoneService.addZone(zoneForm);

        //then
        assertFalse(addedZone.isEmpty());
    }

    @Test
    @DisplayName("shouldThrowWrongInputDataExceptionWhenZoneFormIsNull")
    void addZone_zoneFormIsNull_wrongInputDataException() {
        //given

        //when + then
        assertThrows(WrongInputDataException.class, () -> zoneService.addZone(null));
    }

    @Test
    @DisplayName("shouldCorrectlyGetAnimalsFromGivenZone")
    void getAnimalsFromZone() throws ZoneIsEmptyException, ZoneIdNotExistsException {
        //given
        when(zoneRepository.existsById(Mockito.anyLong())).thenReturn(true);
        when(zoneRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(ZoneMapper.dtoToEntity(testZoneWithAnimals)));

        //when
        final var getAnimals = zoneService.getAnimalsFromZone(testZoneId);

        //then
        assertFalse(getAnimals.isEmpty());
    }

    @Test
    @DisplayName("shouldThrowZoneIdNotExistsExceptionWhenZoneWithGivenIdNotExists")
    void getAnimalsFromZone_zoneIdNotExistsException() {
        //given

        //when + then
        assertThrows(ZoneIdNotExistsException.class, () -> zoneService.getAnimalsFromZone(testZoneId));
    }

    @Test
    @DisplayName("shouldThrowZoneIsEmptyExceptionWhenZoneExistsButHaveLackOfAnimals")
    void getAnimalsFromZone_zoneExists_ZoneIsEmptyException() {
        //given
        when(zoneRepository.existsById(Mockito.anyLong())).thenReturn(true);
        when(zoneRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(ZoneMapper.dtoToEntity(testZone)));

        //when + then
        assertThrows(ZoneIsEmptyException.class, () -> zoneService.getAnimalsFromZone(testZoneId));
    }

    @Test
    @DisplayName("shouldCorrectlyGetZoneWhereMostFoodIsEaten")
    void getZonesWhereMostFoodIsEaten() throws NoAvailableZonesException, ZoneIsEmptyException {
        //given
        when(zoneRepository.count()).thenReturn(oneZoneExists);
        when(zoneRepository.findAll()).thenReturn(ZoneMapper.listDtoToEntity(testZoneWithAnimalList));

        //when
        final var getZones = zoneService.getZonesWhereMostFoodIsEaten();

        //then
        assertFalse(getZones.isEmpty());
    }

    @Test
    @DisplayName("shouldThrowNoAvailableZonesExceptionWhenNoZoneExists")
    void getZonesWhereMostFoodIsEaten_noAvailableZonesException() {
        //given

        //when+then
        assertThrows(NoAvailableZonesException.class, () -> zoneService.getZonesWhereMostFoodIsEaten());
    }

    @Test
    @DisplayName("shouldThrowZoneIsEmptyExceptionWhenZoneExistsButHaveLackOfAnimals")
    void getZonesWhereMostFoodIsEaten_zoneIsEmptyException() {
        //given
        when(zoneRepository.count()).thenReturn(oneZoneExists);
        when(zoneRepository.findAll()).thenReturn(ZoneMapper.listDtoToEntity(testEmptyZoneList));

        //when+then
        assertThrows(ZoneIsEmptyException.class, () -> zoneService.getZonesWhereMostFoodIsEaten());
    }

    @Test
    @DisplayName("shouldCorrectlyGetZoneWithFewestAnimals")
    void getZonesWithFewestAnimals() throws NoAvailableZonesException, ZoneIsEmptyException {
        //given
        when(zoneRepository.count()).thenReturn(oneZoneExists);
        when(zoneRepository.findAll()).thenReturn(ZoneMapper.listDtoToEntity(testZoneWithAnimalList));

        //when
        final var getZones = zoneService.getZonesWithFewestAnimals();

        //then
        assertFalse(getZones.isEmpty());
    }

    @Test
    @DisplayName("shouldThrowNoAvailableZonesExceptionWhenNoZoneExists")
    void getZonesWithFewestAnimals_NoAvailableZonesException() {
        //given

        //when+then
        assertThrows(NoAvailableZonesException.class, () -> zoneService.getZonesWithFewestAnimals());
    }

    @Test
    @DisplayName("shouldThrowZoneIsEmptyExceptionWhenZoneExistsButHaveLackOfAnimals")
    void getZonesWithFewestAnimals_ZoneIsEmptyException() {
        //given
        when(zoneRepository.count()).thenReturn(oneZoneExists);
        when(zoneRepository.findAll()).thenReturn(ZoneMapper.listDtoToEntity(testEmptyZoneList));

        //when+then
        assertThrows(ZoneIsEmptyException.class, () -> zoneService.getZonesWithFewestAnimals());
    }
}