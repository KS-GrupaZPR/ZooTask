package zpr.zootask.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import zpr.zootask.enums.SpeciesEnum;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SpeciesHelperTest {

    @ParameterizedTest
    @EnumSource(SpeciesEnum.class)
    @DisplayName("shouldPassIfCorrectSpeciesAreGiven")
    void checkSpeciesCorrectness_correctSpecies(final SpeciesEnum species) {
        //given

        //when
        final var correctSpecies = SpeciesHelper.INSTANCE.checkSpeciesCorrectness(String.valueOf(species));

        //then
        assertTrue(correctSpecies);
    }

    @Test
    @DisplayName("shouldPassIfIncorrectSpeciesAreGiven")
    void checkSpeciesCorrectness_incorrectSpecies() {
        //given
        final String randomText = "random";

        //when
        final var incorrectSpecies = SpeciesHelper.INSTANCE.checkSpeciesCorrectness(randomText);

        //then
        assertFalse(incorrectSpecies);
    }
}