package zpr.zootask.service;

import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.form.AddZoneForm;
import zpr.zootask.exceptions.*;

import java.util.List;

public interface ZoneService {

    List<AnimalDto> getAnimalsFromZone(long zoneId) throws ZoneIdNotExistsException, ZoneIsEmptyException;

    String getZonesWhereMostFoodIsEaten() throws NoAvailableZonesException, ZoneIsEmptyException;

    String getZonesWithFewestAnimals() throws NoAvailableZonesException, ZoneIsEmptyException;

    String addZone(AddZoneForm addZoneForm) throws IncorrectSpeciesException, WrongInputDataException;
}
