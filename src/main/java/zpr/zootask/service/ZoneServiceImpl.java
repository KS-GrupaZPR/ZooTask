package zpr.zootask.service;

import org.springframework.stereotype.Service;
import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.dto.ZoneDto;
import zpr.zootask.data.form.AddZoneForm;
import zpr.zootask.data.mapper.AnimalMapper;
import zpr.zootask.data.mapper.ZoneMapper;
import zpr.zootask.enums.SpeciesEnum;
import zpr.zootask.exceptions.*;
import zpr.zootask.repository.ZoneRepository;
import zpr.zootask.utils.SpeciesHelper;

import java.util.List;

import static zpr.zootask.interfaces.AnimalConstants.EMPTY_ANIMAL_LIST;
import static zpr.zootask.interfaces.ZoneConstants.*;
import static zpr.zootask.interfaces.ZoneNutritionConstants.FOOD_IS_NOT_CONSUMES;
import static zpr.zootask.interfaces.ZoneNutritionConstants.MAX_FOOD_UNITS_NUMBER;

@Service
public class ZoneServiceImpl implements ZoneService {

    private final ZoneRepository zoneRepository;

    public ZoneServiceImpl(final ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }

    @Override
    public String addZone(final AddZoneForm addZoneForm) throws IncorrectSpeciesException, WrongInputDataException {
        if(addZoneForm == null){
            throw new WrongInputDataException("Wrong input data! Correct format: {\"species\":\"x\"}");
        }
        if (!SpeciesHelper.INSTANCE.checkSpeciesCorrectness(addZoneForm.species())) {
            throw new IncorrectSpeciesException("Zone contains only certain species! Elephant, Lion or Rabbit");
        }

        final var requestSpecies = SpeciesEnum.valueOf(addZoneForm.species().toUpperCase());
        final var newZone = ZoneDto.builder().species(requestSpecies).build();
        final var savedZone = zoneRepository.saveAndFlush(ZoneMapper.dtoToEntity(newZone));
        return savedZone.getSpecies().name();
    }

    @Override
    public List<AnimalDto> getAnimalsFromZone(final long zoneId) throws ZoneIdNotExistsException, ZoneIsEmptyException {
        if (!zoneRepository.existsById(zoneId)) {
            throw new ZoneIdNotExistsException(String.format("Zone '%d' not exists!", zoneId));
        }

        final var zone = zoneRepository.findById(zoneId);
        final var animals = AnimalMapper.listEntityToDtoWithoutZone(zone.get().getAnimals());
        if (animals.size() != EMPTY_ANIMAL_LIST) {
            return animals;
        } else {
            throw new ZoneIsEmptyException(String.format("Zone '%d' is empty!", zoneId));
        }
    }

    @Override
    public String getZonesWhereMostFoodIsEaten() throws NoAvailableZonesException, ZoneIsEmptyException {
        final int INITIAL_COUNT = 0;
        if (!(zoneRepository.count() > NO_AVAILABLE_ZONES)) {
            throw new NoAvailableZonesException("No available zones!");
        }

        final var zones = ZoneMapper.listEntityToDto(zoneRepository.findAll());
        StringBuilder emptyZones = new StringBuilder();
        StringBuilder maxFoodConsumesZones = new StringBuilder();
        long zoneId = INITIAL_COUNT;
        int maxFoodConsumes = INITIAL_COUNT;
        for (ZoneDto zoneList : zones) {
            final var animalsCount = zoneList.getAnimals().size();
            var foodConsumes = animalsCount * zoneList.getSpecies().getFoodConsumes();
            if (zones.size() == SINGLE_ZONE_EXISTS && animalsCount == EMPTY_ANIMAL_LIST) {
                throw new ZoneIsEmptyException(String.format("Zone '%d' is empty!", zoneList.getId()));
            } else if (zones.size() >= SINGLE_ZONE_EXISTS) {
                if (foodConsumes == FOOD_IS_NOT_CONSUMES) {
                    emptyZones.append(",").append(zoneList.getId());
                }
                if (maxFoodConsumes < foodConsumes) {
                    maxFoodConsumes = foodConsumes;
                    zoneId = zoneList.getId();
                }
                if (foodConsumes == MAX_FOOD_UNITS_NUMBER) {
                    maxFoodConsumes = foodConsumes;
                    maxFoodConsumesZones.append(",").append(zoneList.getId());
                }
            }
        }
        if (maxFoodConsumes == FOOD_IS_NOT_CONSUMES) {
            return String.format("All existing zones '%s' are empty!", emptyZones.substring(1));
        }
        if (maxFoodConsumes == MAX_FOOD_UNITS_NUMBER) {
            return String.format("Most food '%d' is eaten in zones '%s'", maxFoodConsumes, maxFoodConsumesZones.substring(1));
        }
        return String.format("Most food '%d' is eaten in zone '%d'", maxFoodConsumes, zoneId);
    }

    @Override
    public String getZonesWithFewestAnimals() throws NoAvailableZonesException, ZoneIsEmptyException {
        if (!(zoneRepository.count() > NO_AVAILABLE_ZONES)) {
            throw new NoAvailableZonesException("No available zones!");
        }
        final var zones = ZoneMapper.listEntityToDto(zoneRepository.findAll());
        StringBuilder emptyZones = new StringBuilder();
        StringBuilder minAnimalsLiveZones = new StringBuilder();
        var minAnimalsLive = checkMinNumberOfAnimals();
        for (ZoneDto zoneList : zones) {
            final var animalsCount = zoneList.getAnimals().size();
            if (minAnimalsLive != MIN_NUMBER_OF_LIVING_ANIMALS && animalsCount != EMPTY_ANIMAL_LIST) {
                if (animalsCount == minAnimalsLive) {
                    minAnimalsLiveZones.append(",").append(zoneList.getId());
                }
            } else {
                if (zones.size() == SINGLE_ZONE_EXISTS && animalsCount == EMPTY_ANIMAL_LIST) {
                    throw new ZoneIsEmptyException(String.format("Zone '%d' is empty!", zoneList.getId()));
                } else if (zones.size() >= SINGLE_ZONE_EXISTS) {
                    if (animalsCount == EMPTY_ANIMAL_LIST) {
                        emptyZones.append(",").append(zoneList.getId());
                    }
                    if (animalsCount == MIN_NUMBER_OF_LIVING_ANIMALS) {
                        minAnimalsLiveZones.append(",").append(zoneList.getId());
                    }
                }
            }
        }
        if (!minAnimalsLiveZones.isEmpty()) {
            return String.format("In '%s' zone lives fewest animal '%d'",
                    minAnimalsLiveZones.substring(1),
                    minAnimalsLive);
        }
        return String.format("In '%s' zone no animals live in!", emptyZones.substring(1));
    }

    private int checkMinNumberOfAnimals() {
        final var zones = ZoneMapper.listEntityToDto(zoneRepository.findAll());
        var minAnimalsLive = MAX_NUMBER_OF_LIVING_ELEPHANTS;
        if (zones.size() > NO_AVAILABLE_ZONES)
            for (ZoneDto zoneList : zones) {
                var animalsCount = zoneList.getAnimals().size();
                if (animalsCount < minAnimalsLive) {
                    minAnimalsLive = animalsCount;
                }
            }
        return minAnimalsLive;
    }
}
