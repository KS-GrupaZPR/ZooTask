package zpr.zootask.service;

import org.springframework.stereotype.Service;
import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.dto.ZoneDto;
import zpr.zootask.data.entity.AnimalEntity;
import zpr.zootask.data.form.AddAnimalForm;
import zpr.zootask.data.mapper.AnimalMapper;
import zpr.zootask.data.mapper.ZoneMapper;
import zpr.zootask.enums.SpeciesEnum;
import zpr.zootask.exceptions.*;
import zpr.zootask.interfaces.ZoneConstants;
import zpr.zootask.repository.AnimalRepository;
import zpr.zootask.repository.ZoneRepository;
import zpr.zootask.utils.SpeciesHelper;

import java.util.Objects;

@Service
public class AnimalServiceImpl implements AnimalService {

    private final ZoneRepository zoneRepository;
    private final AnimalRepository animalRepository;

    public AnimalServiceImpl(final ZoneRepository zoneRepository, final AnimalRepository animalRepository) {
        this.zoneRepository = zoneRepository;
        this.animalRepository = animalRepository;
    }

    @Override
    public AnimalDto getAnimalWithName(final String name) throws AnimalNameNotExistsException {
        if (!animalRepository.existsByName(name)) {
            throw new AnimalNameNotExistsException(String.format("Animal named '%s' not exists!", name));
        }
        return AnimalMapper.entityToDto(animalRepository.findByName(name));
    }

    @Override
    public String addAnimal(final AddAnimalForm addAnimalForm) throws IncorrectSpeciesException,
            ZoneNotFoundException, AnimalNameExistsException, WrongInputDataException {
        if(addAnimalForm == null){
            throw new WrongInputDataException("Wrong input data! Correct format: {\"name\":\"x\",\"species\":\"x\"}");
        }

        if (!SpeciesHelper.INSTANCE.checkSpeciesCorrectness(addAnimalForm.species())) {
            throw new IncorrectSpeciesException(
                    "Zone contains only certain species! Elephant, Lion or Rabbit");
        }

        final var requestSpecies = SpeciesEnum.valueOf(addAnimalForm.species().toUpperCase());
        if (!checkIfThereIsAAvailableZoneWithAGivenSpecies(requestSpecies)) {
            throw new ZoneNotFoundException(
                    String.format("Available zone for species '%s' not found!", requestSpecies));
        }

        if (checkIfAnimalNameExistsInDatabase(addAnimalForm.name())) {
            throw new AnimalNameExistsException(
                    String.format("%s name '%s' already exists!", requestSpecies, addAnimalForm.name()));
        }

        final var zone = ZoneMapper.entityToDto(
                zoneRepository.findFirstBySpeciesAndBlockZoneFalse(requestSpecies));
        try {
            zone.checkFoodLimit(requestSpecies.name());
            final var savedAnimal =
                    Objects.requireNonNull(saveAnimal(addAnimalForm.name(), requestSpecies, zone));
            zone.addAnimal(savedAnimal);
            final var savedZone =
                    zoneRepository.saveAndFlush(ZoneMapper.dtoToEntity(zone));
            return String.format(
                    "Animal '%s' added to zone '%d:%s'", savedAnimal.getName(),
                    savedZone.getId(), savedZone.getSpecies().name());
        } catch (FoodLimitExceededException exc) {
            zone.setBlockZone(ZoneConstants.BLOCKED_ZONE);
            zoneRepository.saveAndFlush(ZoneMapper.dtoToEntity(zone));
            return exc.getMessage();
        }
    }

    private boolean checkIfThereIsAAvailableZoneWithAGivenSpecies(final SpeciesEnum species) {
        if (!zoneRepository.existsBySpecies(species)) {
            return false;
        }
        return zoneRepository.findFirstBySpeciesAndBlockZoneFalse(species) != null;
    }

    private boolean checkIfAnimalNameExistsInDatabase(final String name) {
        return animalRepository.existsByName(name);
    }

    private AnimalDto saveAnimal(final String name, final SpeciesEnum species, final ZoneDto zoneDto) {
        return AnimalMapper.entityToDto(
                animalRepository.saveAndFlush(
                        new AnimalEntity(name, species.name(), ZoneMapper.dtoToEntity(zoneDto))));
    }
}
