package zpr.zootask.service;

import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.form.AddAnimalForm;
import zpr.zootask.exceptions.*;

public interface AnimalService {

    AnimalDto getAnimalWithName(String name) throws AnimalNameNotExistsException;

    String addAnimal(AddAnimalForm addAnimalForm) throws IncorrectSpeciesException,
            ZoneNotFoundException, AnimalNameExistsException, WrongInputDataException;
}
