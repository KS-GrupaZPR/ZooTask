package zpr.zootask.exceptions;

public class FoodLimitExceededException extends Exception {

    public FoodLimitExceededException(final String message) {
        super(message);
    }
}
