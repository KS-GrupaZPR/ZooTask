package zpr.zootask.exceptions;

public class ZoneNotFoundException extends Exception {

    public ZoneNotFoundException(final String message) {
        super(message);
    }
}
