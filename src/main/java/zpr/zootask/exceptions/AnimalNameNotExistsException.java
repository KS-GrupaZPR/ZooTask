package zpr.zootask.exceptions;

public class AnimalNameNotExistsException extends Exception {

    public AnimalNameNotExistsException(final String message) {
        super(message);
    }
}
