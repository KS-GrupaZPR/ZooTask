package zpr.zootask.exceptions;

public class ZoneIsEmptyException extends Exception {

    public ZoneIsEmptyException(final String message) {
        super(message);
    }
}
