package zpr.zootask.exceptions;

public class WrongInputDataException extends Exception {

    public WrongInputDataException(final String message) {
        super(message);
    }
}
