package zpr.zootask.exceptions;

public class AnimalNameExistsException extends Exception {

    public AnimalNameExistsException(final String message) {
        super(message);
    }
}
