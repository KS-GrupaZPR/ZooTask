package zpr.zootask.exceptions;

public class IncorrectSpeciesException extends Exception {

    public IncorrectSpeciesException(final String message) {
        super(message);
    }
}
