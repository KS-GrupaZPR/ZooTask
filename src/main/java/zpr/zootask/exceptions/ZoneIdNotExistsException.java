package zpr.zootask.exceptions;

public class ZoneIdNotExistsException extends Exception {

    public ZoneIdNotExistsException(final String message) {
        super(message);
    }
}
