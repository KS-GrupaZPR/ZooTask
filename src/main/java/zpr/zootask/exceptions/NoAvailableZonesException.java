package zpr.zootask.exceptions;

public class NoAvailableZonesException extends Exception {

    public NoAvailableZonesException(final String message) {
        super(message);
    }
}
