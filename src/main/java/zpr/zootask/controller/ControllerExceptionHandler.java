package zpr.zootask.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import zpr.zootask.exceptions.*;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler({ZoneNotFoundException.class, ZoneIdNotExistsException.class,
            ZoneIsEmptyException.class, NoAvailableZonesException.class})
    public ResponseEntity<String> zoneExcHandler(Exception exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getMessage());
    }

    @ExceptionHandler({AnimalNameExistsException.class, AnimalNameNotExistsException.class})
    public ResponseEntity<String> animalExcHandler(Exception exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getMessage());
    }

    @ExceptionHandler({IncorrectSpeciesException.class, WrongInputDataException.class})
    public ResponseEntity<String> zoneAndAnimalExcHandler(Exception exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> wrongUserInputExcHandler(MethodArgumentNotValidException exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getFieldError().getDefaultMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> wrongUserInputExcHandler() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request body is missing!");
    }
}
