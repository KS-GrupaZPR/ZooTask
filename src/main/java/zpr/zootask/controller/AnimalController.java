package zpr.zootask.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.form.AddAnimalForm;
import zpr.zootask.exceptions.*;
import zpr.zootask.service.AnimalService;

@RestController
@RequestMapping("/api/animals")
public class AnimalController {

    private final AnimalService animalService;

    public AnimalController(final AnimalService animalService) {
        this.animalService = animalService;
    }

    @GetMapping(value = "/{name}")
    public ResponseEntity<AnimalDto> getAnimalWithName(@PathVariable(name = "name") String animalName) throws
            AnimalNameNotExistsException {
        return ResponseEntity.status(HttpStatus.OK).body(animalService.getAnimalWithName(animalName));
    }

    @PostMapping()
    public ResponseEntity<String> addAnimal(@Valid @RequestBody AddAnimalForm addAnimalForm) throws
            IncorrectSpeciesException, ZoneNotFoundException, AnimalNameExistsException, WrongInputDataException {
        return ResponseEntity.status(HttpStatus.OK).body(animalService.addAnimal(addAnimalForm));
    }
}
