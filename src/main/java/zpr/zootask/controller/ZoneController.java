package zpr.zootask.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.form.AddZoneForm;
import zpr.zootask.exceptions.*;
import zpr.zootask.service.ZoneServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/api/zones")
public class ZoneController {

    private final ZoneServiceImpl zoneService;

    public ZoneController(final ZoneServiceImpl zoneService) {
        this.zoneService = zoneService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<AnimalDto>> getAnimalsFromZone(@PathVariable(name = "id") int zoneId) throws
            ZoneIdNotExistsException, ZoneIsEmptyException {
        return ResponseEntity.status(HttpStatus.OK).body(zoneService.getAnimalsFromZone(zoneId));
    }

    @GetMapping("/food")
    public ResponseEntity<String> getZonesWhereMostFoodIsEaten() throws
            NoAvailableZonesException, ZoneIsEmptyException {
        return ResponseEntity.status(HttpStatus.OK).body(zoneService.getZonesWhereMostFoodIsEaten());
    }

    @GetMapping()
    public ResponseEntity<String> getZonesWithFewestAnimals() throws
            NoAvailableZonesException, ZoneIsEmptyException {
        return ResponseEntity.status(HttpStatus.OK).body(zoneService.getZonesWithFewestAnimals());
    }

    @PostMapping()
    public ResponseEntity<String> addZone(@Valid @RequestBody AddZoneForm addZoneForm) throws
            IncorrectSpeciesException, WrongInputDataException {
        return ResponseEntity.status(HttpStatus.OK).body(
                String.format("Zone added, with species: %s ", zoneService.addZone(addZoneForm)));
    }
}
