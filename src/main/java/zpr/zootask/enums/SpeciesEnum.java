package zpr.zootask.enums;

import lombok.Getter;

public enum SpeciesEnum {
    ELEPHANT(20),
    LION(11),
    RABBIT(4);

    @Getter
    private final int foodConsumes;

    SpeciesEnum(int foodConsumes) {
        this.foodConsumes = foodConsumes;
    }
}
