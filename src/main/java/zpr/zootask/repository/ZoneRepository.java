package zpr.zootask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zpr.zootask.data.entity.ZoneEntity;
import zpr.zootask.enums.SpeciesEnum;

@Repository
public interface ZoneRepository extends JpaRepository<ZoneEntity, Long> {

    boolean existsBySpecies(SpeciesEnum species);

    ZoneEntity findFirstBySpeciesAndBlockZoneFalse(SpeciesEnum species);
}
