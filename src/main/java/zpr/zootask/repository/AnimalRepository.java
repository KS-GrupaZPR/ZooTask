package zpr.zootask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zpr.zootask.data.entity.AnimalEntity;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalEntity, Long> {

    boolean existsByName(String name);

    AnimalEntity findByName(String name);
}
