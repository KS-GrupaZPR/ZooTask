package zpr.zootask.interfaces;

public interface ZoneConstants {
    boolean BLOCKED_ZONE = Boolean.TRUE;
    int NO_AVAILABLE_ZONES = 0;
    int SINGLE_ZONE_EXISTS = 1;
    int MIN_NUMBER_OF_LIVING_ANIMALS = 1;
    int MAX_NUMBER_OF_LIVING_ELEPHANTS = 5;
}
