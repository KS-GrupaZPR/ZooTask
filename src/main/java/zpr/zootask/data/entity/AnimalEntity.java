package zpr.zootask.data.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter

@Entity
@Table(name = "animals")
public class AnimalEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Getter
    @Column(length = 30)
    private String name;
    private String type;
    @ManyToOne
    private ZoneEntity zone;

    public AnimalEntity(String name, String type, ZoneEntity zone) {
        this.name = name;
        this.type = type;
        this.zone = zone;
    }
}
