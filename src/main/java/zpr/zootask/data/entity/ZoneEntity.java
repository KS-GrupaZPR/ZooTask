package zpr.zootask.data.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import zpr.zootask.enums.SpeciesEnum;
import zpr.zootask.interfaces.ZoneConstants;
import zpr.zootask.interfaces.ZoneNutritionConstants;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter

@Entity
@Table(name = "zones")
public class ZoneEntity implements ZoneNutritionConstants, ZoneConstants {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private SpeciesEnum species;
    private boolean blockZone;
    @OneToMany(mappedBy = "zone")
    @JsonBackReference
    private List<AnimalEntity> animals;
}
