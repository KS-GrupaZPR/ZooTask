package zpr.zootask.data.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import zpr.zootask.data.entity.ZoneEntity;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class AnimalDto {
    private long id;
    private String name;
    private String type;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ZoneEntity zone;
}
