package zpr.zootask.data.dto;

import lombok.*;
import zpr.zootask.enums.SpeciesEnum;
import zpr.zootask.exceptions.FoodLimitExceededException;
import zpr.zootask.interfaces.ZoneConstants;
import zpr.zootask.interfaces.ZoneNutritionConstants;

import java.util.List;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class ZoneDto implements ZoneNutritionConstants, ZoneConstants {
    private long id;
    private SpeciesEnum species;
    @Setter
    private boolean blockZone;
    private List<AnimalDto> animals;

    public void checkFoodLimit(final String animalType) throws FoodLimitExceededException {
        final var quantity = SpeciesEnum.valueOf(animalType).getFoodConsumes();
        var checkZoneLimit = animals.size() * quantity;
        checkZoneLimit += quantity;
        if (checkZoneLimit > MAX_FOOD_UNITS_NUMBER) {
            throw new FoodLimitExceededException("Food limit in zone exceeded!");
        }
    }

    public void addAnimal(final AnimalDto animal) {
        final var quantity = SpeciesEnum.valueOf(animal.getType()).getFoodConsumes();
        animals.add(animal);
        if ((animals.size() * quantity) == MAX_FOOD_UNITS_NUMBER) {
            blockZone = BLOCKED_ZONE;
        }
    }
}
