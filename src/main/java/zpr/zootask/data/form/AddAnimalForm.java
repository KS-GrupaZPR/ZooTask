package zpr.zootask.data.form;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public record AddAnimalForm(
        @NotNull(message = "Name has to be provided!")
        @NotEmpty(message = "Name has to be provided!")
        @Length(min = 4, max = 30, message = "Name has to have at least 4 characters and up to 30 characters!")
        String name,
        @NotNull(message = "Species has to be provided!")
        @NotEmpty(message = "Species has to be provided!")
        String species) {
}
