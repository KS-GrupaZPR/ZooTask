package zpr.zootask.data.form;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record AddZoneForm(
        @NotNull(message = "Species has to be provided!")
        @NotEmpty(message = "Species has to be provided!")
        String species) {
}
