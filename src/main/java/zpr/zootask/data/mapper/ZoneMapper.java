package zpr.zootask.data.mapper;

import zpr.zootask.data.dto.ZoneDto;
import zpr.zootask.data.entity.ZoneEntity;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ZoneMapper {

    public static ZoneDto entityToDto(final ZoneEntity zoneEntity) {
        return ZoneDto.builder()
                .id(zoneEntity.getId())
                .species(zoneEntity.getSpecies())
                .blockZone(zoneEntity.isBlockZone())
                .animals(AnimalMapper.listEntityToDto(
                        zoneEntity.getAnimals() == null ? Collections.emptyList() : zoneEntity.getAnimals()))
                .build();
    }

    public static ZoneEntity dtoToEntity(final ZoneDto zoneDto) {
        return new ZoneEntity(
                zoneDto.getId(),
                zoneDto.getSpecies(),
                zoneDto.isBlockZone(),
                AnimalMapper.listDtoToEntity(
                        zoneDto.getAnimals() == null ? Collections.emptyList() : zoneDto.getAnimals()));
    }

    public static List<ZoneDto> listEntityToDto(final List<ZoneEntity> zoneEntityList) {
        return zoneEntityList.stream().map(ZoneMapper::entityToDto).collect(Collectors.toList());
    }

    public static List<ZoneEntity> listDtoToEntity(final List<ZoneDto> zoneDtoList) {
        return zoneDtoList.stream().map(ZoneMapper::dtoToEntity).collect(Collectors.toList());
    }
}
