package zpr.zootask.data.mapper;


import zpr.zootask.data.dto.AnimalDto;
import zpr.zootask.data.entity.AnimalEntity;

import java.util.List;
import java.util.stream.Collectors;

public class AnimalMapper {

    public static AnimalDto entityToDto(final AnimalEntity animalEntity) {
        return AnimalDto.builder()
                .id(animalEntity.getId())
                .name(animalEntity.getName())
                .type(animalEntity.getType())
                .zone(animalEntity.getZone())
                .build();
    }

    public static AnimalDto entityToDtoWithoutZone(final AnimalEntity animalEntity) {
        return AnimalDto.builder()
                .id(animalEntity.getId())
                .name(animalEntity.getName())
                .type(animalEntity.getType())
                .build();
    }

    public static AnimalEntity dtoToEntity(final AnimalDto animalDto) {
        return new AnimalEntity(
                animalDto.getId(),
                animalDto.getName(),
                animalDto.getType(),
                animalDto.getZone());
    }

    public static List<AnimalDto> listEntityToDto(final List<AnimalEntity> animalEntityList) {
        return animalEntityList.stream().map(AnimalMapper::entityToDto).collect(Collectors.toList());
    }

    public static List<AnimalDto> listEntityToDtoWithoutZone(final List<AnimalEntity> animalEntityList) {
        return animalEntityList.stream().map(AnimalMapper::entityToDtoWithoutZone).collect(Collectors.toList());
    }

    public static List<AnimalEntity> listDtoToEntity(final List<AnimalDto> animalDtoList) {
        return animalDtoList.stream().map(AnimalMapper::dtoToEntity).collect(Collectors.toList());
    }
}
