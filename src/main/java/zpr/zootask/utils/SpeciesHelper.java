package zpr.zootask.utils;

import zpr.zootask.enums.SpeciesEnum;

import java.util.Arrays;

public enum SpeciesHelper {

    INSTANCE;

    public boolean checkSpeciesCorrectness(final String requestSpecies) {
        try {
            return Arrays.stream(SpeciesEnum.values())
                    .anyMatch(species -> species.equals(SpeciesEnum.valueOf(requestSpecies.toUpperCase())));
        } catch (IllegalArgumentException exc) {
            return false;
        }
    }
}
