package zpr.zootask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZooTaskApplication {

    public static void main(String[] args) {
        System.out.println("Kamil Stencel, GrupaZPR ZooTask");
        SpringApplication.run(ZooTaskApplication.class, args);
    }

}
